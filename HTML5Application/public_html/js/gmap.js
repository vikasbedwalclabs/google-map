var geocoder;
var map;
var marker;
var markers = [];
var input = document.getElementById('location');
var searchForm = document.getElementById('gmap');
var place;
var autoComplete = new google.maps.places.Autocomplete(input);
//Add listener to detect autocomplete selection
google.maps.event.addListener(autoComplete, 'place_changed', function() {
    place = autoComplete.getPlace();
});
//Add listener to search
searchForm.addEventListener("submit", function() {
    var newlatlong = new google.maps.LatLng(place.geometry.location.lat(), place.geometry.location.lng());
    map.setCenter(newlatlong);
    marker.setPosition(newlatlong);
    map.setZoom(7);
    document.getElementById("lat").value = place.geometry.location.lat();
    document.getElementById("lng").value = place.geometry.location.lng();
    clearMarkers();    
    markers.push(marker);
    placeMarker(newlatlong);
});
//Reset the inpout box on click
input.addEventListener('click', function() {
    input.value = "";
});
var myCenter = new google.maps.LatLng(21, 78);
function initialize() {
    geocoder = new google.maps.Geocoder();
    var mapProp = {
        center: myCenter,
        zoom: 5,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    map = new google.maps.Map(document.getElementById("google-map"), mapProp);
    google.maps.event.addListener(map, 'click', function(event) {
        placeMarker(event.latLng);
    });
    marker = new google.maps.Marker({
       // position: myCenter,
        map: map,
        title: 'Main map'
    });
}
google.maps.event.addDomListener(window, 'load', initialize);
// Function to set marker on click
function placeMarker(l) {
    var marker = new google.maps.Marker({
        position: l,
        map: map
    });
    document.getElementById("lat").value = l.lat();
    document.getElementById("lng").value = l.lng();
    var infowindow = new google.maps.InfoWindow({
        content: 'Latitude: ' + l.lat() + '<br>Longitude: ' + l.lng()
    });
    infowindow.open(map, marker);
// Setting location name into header field 
    var geocoder;
    geocoder = new google.maps.Geocoder();
    var lat = parseFloat(l.lat());
    var lng = parseFloat(l.lng());
    var latlng = new google.maps.LatLng(lat, lng);
    geocoder.geocode({'latLng': latlng}, function(results, status) {
        if (status === google.maps.GeocoderStatus.OK) {
            var r = results[1].formatted_address;
            document.getElementById("location").value = r;
        }
        else
        {
            alert('No Location Found');
        }
    });
    clearMarkers();
    markers.push(marker);
}
// Clears the previous marker
function clearMarkers() {
    for (var i = 0; i < markers.length; i++)
    {
        markers[i].setMap(null);
    }
}
